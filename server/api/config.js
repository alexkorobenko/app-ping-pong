module.exports = {
    'playerHealth': 10,
    'minCountPlayersInRoom': 2,
    'maxCountPlayersInRoom': 2,
    'canvas': {
        'size'  : 600,
        'color' : 'black',
        'positions': {
            1 : {'title' : 'left',},
            2 : {'title' : 'right',},
            3 : {'title' : 'top',},
            4 : {'title' : 'bottom',},
        },
    },
    'ball': {
      'size': 6,
      'color': 'white',
    },
    'control': {
        'size'  : 8, //width
        'k'     : 4, //height = canvas height/k
        'offset': 10, //offset x or y
        'color' : 'white',
        'activeColor': 'orange',
    },
};
