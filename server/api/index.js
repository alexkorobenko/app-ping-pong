const config = require('./config');
const store = require('./store');
const _math = require('./math');

module.exports = class Api {
    constructor(io, socket) {
        this.io = io;
        this.socket = socket;
    }

    enter(data) {
        if (!data || !data['player_name'] || !data['room_name'])
            return _math.serverCallback(this.socket, 'message', 'error', 'Invalid data.');

        let playerName = data['player_name'],
            roomName = data['room_name'],
            playerId = data['player_role'] === 'player' ? this.socket['id'] : data['player_name'],
            playerRole = data['player_role'],
            room = store.isExistRoom(roomName) ? store.getRoom(roomName) : store.createRoom(roomName);

        if (room.state !== 'idle' || room.countPlayers >= config.maxCountPlayersInRoom)
            return _math.serverCallback(this.socket, 'message', 'error', 'In room started game or maxCountPlayersInRoom');

        let player = store.createPlayer(playerId, playerName, playerRole);

        if (!room.io) room.io = this.io;
        room.addPlayer(player);

        this.socket.join(roomName);

        _math.serverCallback(this.socket, 'enter', 'success', {'config': config, 'player': player});

        //send all sockets in room
        _math.serverCallback(this.io.in(roomName), 'update', 'success', room.data);
    }

    disconnect() {
        let playerId = this.socket.id;

        if (!playerId || !store.isExistPlayer(playerId))
            return _math.serverCallback(this.socket, 'message', 'error', 'Invalid data.');

        let player = store.getPlayer(playerId),
            roomName = player.roomName,
            room = store.getRoom(roomName);

        store.removePlayer(playerId);

        let countPlayers = room.countPlayers;
        let countBotPlayers = room.getPlayers('role', 'bot').length;

        if (countPlayers && countPlayers !== countBotPlayers) {
            if (countPlayers === 1) room.stopGame();
        } else {
            store.removeRoom(roomName);
        }
    }

    setPlayerPosition(data) {
        if (!data || !data['player_id'] || !data['position'] || !store.isExistPlayer(data['player_id']))
            return _math.serverCallback(this.socket, 'message', 'error', 'Invalid data.');

        let position = parseInt(data['position']);
        let player = store.getPlayer(data['player_id']);
        let room = store.getRoom(player.roomName);

        if (room.state !== 'idle')
            return _math.serverCallback(this.socket, 'message', 'error', 'In room started game');

        let checkPlayerPosition = room.setPlayerPosition(position);

        if (!checkPlayerPosition)
            return _math.serverCallback(this.socket, 'message', 'error', 'Check other position.');

        let x, y, width, height, color;
        ({x, y, width, height, color} = room.positions[position]['data']);

        player.position = position;
        player.createControl([x, y, width, height, color]);

        _math.serverCallback(this.socket, 'ready', 'success', {'player': player,});

        //send all sockets in room
        _math.serverCallback(this.io.in(player.roomName), 'update', 'success', room.data);
    }

    unsetPlayerPosition(playerId) {
        if (!playerId || !store.isExistPlayer(playerId))
            return _math.serverCallback(this.socket, 'message', 'error', 'Invalid data.');

        let player = store.getPlayer(playerId);
        let room = store.getRoom(player.roomName);

        if (room.state !== 'idle')
            return _math.serverCallback(this.socket, 'message', 'error', 'In room started game');

        room.unsetPlayerPosition(player.position);
        player.destroyControl();

        _math.serverCallback(this.socket, 'changePlayerPosition', 'success', {'player': player});

        //send all sockets in room
        _math.serverCallback(this.io.in(player.roomName), 'update', 'success', room.data);
    }

    removePlayer(playerId) {
        if (!playerId || !store.isExistPlayer(playerId))
            return _math.serverCallback(this.socket, 'message', 'error', 'Invalid data.');

        let player = store.getPlayer(playerId);
        let room = store.getRoom(player.roomName);

        if (room.state !== 'idle')
            return _math.serverCallback(this.socket, 'message', 'error', 'In room started game');

        store.isExistPlayer(playerId) && store.removePlayer(playerId);
    }

    updateControl(playerData) {
        if (!playerData || !store.isExistPlayer(playerData.id))
            return _math.serverCallback(this.socket, 'message', 'error', 'Invalid data.');

        let player = store.getPlayer(playerData.id);
        player.updateControl(playerData.control);
    }

    startGame(roomName) {
        if (!roomName || !store.isExistRoom(roomName))
            return _math.serverCallback(this.socket, 'message', 'error', 'Invalid data.');

        let room = store.getRoom(roomName);

        if (room.state === 'game')
            return _math.serverCallback(this.socket, 'message', 'error', 'In room started game');

        room.startGame();
    }
}