const Room = require('../classes/Room');
const Player = require('../classes/Player');

module.exports = {
    'rooms': {}, //roomName: new instance Room

    'players': {}, //idPlayer: new instance Player

    'isExistRoom': function (name) {
        return Object.keys(this.rooms).indexOf(name) !== -1;
    },

    'isExistPlayer': function (id) {
        return Object.keys(this.players).indexOf(id) !== -1;
    },

    'getRoom': function (name) {
        return this.rooms[name];
    },

    'getPlayer': function (id) {
        return this.players[id];
    },

    'removeRoom': function (name) {
        this.rooms[name].timer.stop();
        delete this.rooms[name];
    },

    'removePlayer': function (id) {
        let player = this.getPlayer(id);

        if (player) {
            let room = this.rooms[player.roomName];

            room.removePlayer(id);
            room.update()

            delete this.players[id];
        }
    },

    'createRoom': function (name) {
        let room = new Room(name);
        this.rooms[name] = room;

        return room;
    },

    'createPlayer': function (id, name, role = 'player') {
        let player = new Player(id, name, role);
        this.players[id] = player;

        return player;
    },
};
