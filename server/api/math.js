const config = require('./config');

class Vec {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }
}

class Circle {
    constructor(x = 0, y = 0, r = 2, color = 'black') {
        this.vec = new Vec(x, y);
        this.r = r;
        this.color = color;
    }

    get top() {
        return this.vec.y - this.r;
    }

    get bottom() {
        return this.vec.y + this.r;
    }

    get left() {
        return this.vec.x - this.r;
    }

    get right() {
        return this.vec.x + this.r;
    }
}

class Rect {
    constructor(x = 0, y = 0, width = 10, height = 10, color = 'black') {
        this.vec = new Vec(x, y);
        this.width = width;
        this.height = height;
        this.color = color;
    }

    get top() {
        return this.vec.y;
    }

    get bottom() {
        return this.vec.y + this.height;
    }

    get left() {
        return this.vec.x;
    }

    get right() {
        return this.vec.x + this.width;
    }
}

function serverCallback(socket, type, status, data) {
    return socket.emit('serverCallback', {
        'type': type,
        'status': status,
        'data': data
    });
}

function calcPlayerPosition(position) {
    if (!position) return  false;

    position = parseInt(position);

    let data = {
        'x': 0,
        'y': 0,
        'width': 0,
        'height': 0,
        'color': config.control.color,
    };

    if (position <= config.minCountPlayersInRoom) {
        data.width = config.control.size;
        data.height = config.canvas.size / config.control.k;
        data.x = (position === 1) ? config.control.offset : config.canvas.size - data.width - config.control.offset;
        data.y = (config.canvas.size - data.height) / 2;

    } else if (position > config.minCountPlayersInRoom) {
        data.width = config.canvas.size / config.control.k;
        data.height = config.control.size;
        data.x = (config.canvas.size - data.width) / 2;
        data.y = (position === 3) ? config.control.offset : config.canvas.size - data.height - config.control.offset;
    }

    return data;
}

function forEach(data, callback) {
    for (let key in data) {
        if (data.hasOwnProperty(key) && callback) callback(data[key], key);
    }
}

module.exports = {
    'Vec': Vec,
    'Circle': Circle,
    'Rect': Rect,
    'serverCallback': serverCallback,
    'calcPlayerPosition': calcPlayerPosition,
    'forEach': forEach
};
