const express = require('express');
const socketIO = require('socket.io');
const path = require('path');
const http = require('http');
const isDev = process.env.NODE_ENV === 'development';

const publicPath = path.join(__dirname, '../dist');
const port = process.env.PORT || 5000;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const Api = require('./api/index');

app.use(express.static(publicPath));

io.on('connection', socket => {
    const gameApi = new Api(io, socket);

    socket.on('enter', data => {
        gameApi.enter(data);
    });

    socket.on('disconnect', () => {
        gameApi.disconnect();
    });

    socket.on('setPlayerPosition', data => {
        gameApi.setPlayerPosition(data);
    });

    socket.on('unsetPlayerPosition', playerId => {
        gameApi.unsetPlayerPosition(playerId);
    });

    socket.on('removePlayer', playerId => {
        gameApi.removePlayer(playerId);
    });

    socket.on('updateControl', player => {
        gameApi.updateControl(player);
    });

    socket.on('startGame', roomName => {
        gameApi.startGame(roomName);
    });
});

if (isDev) {
    const ip = require('ip');
    const ipAddress = ip.address();

    server.listen(port, ipAddress,() => {
        console.log(`Server started on: http://${ipAddress}:${port}`)
    })
} else {
    server.listen(port)
}