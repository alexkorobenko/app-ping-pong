const config = require('../api/config');
const Control = require('./Control');

module.exports = class Player {
    constructor (id, name, role = 'player') {
        this.state = 'idle'; //idle, die
        this.id = id;
        this.name = name;
        this.roomName = null;
        this.health = null;
        this.control = null; //new instance Control
        this.role = role; //player or bot
        this.position = null; //1,2,3,4

        this.resetHealth();
    }

    createControl(data) {
        this.control = new Control(...data);
    }

    updateControl(data) {
        this.control.update(data);
    }

    resetControl() {
        this.control.reset();
    }

    destroyControl() {
      this.control = null;
      this.position = null;
    }

    incrementHealth() {
        this.health -= 1;
        if (!this.health) this.state = 'die';
    }

    resetHealth() {
        this.state = 'idle';
        this.health = config.playerHealth;
    }

    reset() {
        this.resetHealth();
        this.resetControl();
    }
};
