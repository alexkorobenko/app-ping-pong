module.exports = class Timer {
  constructor() {
      this.tick = null;
      this.lastTime = null;
  }

  start() {
      if (this.tick) return;

      this.reset();

      this.tick = setInterval(() => {
          let time = new Date().getTime();
          let delta = time - this.lastTime; // get the delta time since last frame

          this.lastTime = time;
          this.update(delta/60);
      }, 1000 / 60);
  }

  stop() {
      if (!this.tick) return;
      clearInterval(this.tick);
      this.tick = null;
  }

  reset() {
      this.lastTime = new Date().getTime();
  }
};