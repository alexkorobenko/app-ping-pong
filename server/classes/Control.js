const _math = require('../api/math');

module.exports = class Control extends _math.Rect {
    constructor(x = 0, y = 0, width = 10, height = 10, color = 'black') {
        super(x, y, width, height, color);
        this.vecOrigin = new _math.Vec(x, y);
    }

    update(data) {
        for (let key in data) {
            if (data.hasOwnProperty(key)) this[key] = data[key];
        }
    }

    reset() {
        this.vec.x = this.vecOrigin.x;
        this.vec.y = this.vecOrigin.y;
    }
};
