const _math = require('../api/math');

module.exports = class Ball extends _math.Circle {
    constructor(x = 0, y = 0, r = 4, color = 'white') {
        super(x, y, r, color);
        this.state = 'wait'; //idle, wait
        this.vecOrigin = new _math.Vec(x, y);
        this.vel = new _math.Vec(
            25 * (Math.random() > 0.5 ? 1 : -1),
            35
        );
        this.direction = {'x': null, 'y': null};
    }

    update(time) {
        if (this.state !== 'idle') return;
        let vx = this.vel.x * time;
        let vy = this.vel.y * time;

        this.vec.x += vx;
        this.vec.y += vy;
        this.direction = {'x': vx > 0 ? 1 : -1, 'y': vy > 0 ? 1 : -1};
    }

    enable() {
        this.state = 'idle';
    }

    disable() {
        this.state = 'wait';
    }

    reset() {
        this.vel.x = 25 * (Math.random() > 0.5 ? 1 : -1);
        this.vec.x = this.vecOrigin.x;
        this.vec.y = this.vecOrigin.y;
        this.direction = {'x': null, 'y': null};
    }
};
