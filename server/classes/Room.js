const config = require('../api/config');
const _math = require('../api/math');
const Ball = require('./Ball');
const Timer = require('./Timer');

module.exports = class Room {
    constructor (name) {
        this.state = 'idle'; //idle, wait, game
        this.name = name; //room name
        this.players = {}; //playerId: new instance Player
        this.positions = {};
        this.ball = null; //new instance Ball
        this.timer = null; //new instance Timer
        this.io = null; //io socket

        //init
        for (let i = 1; i <= 4; i +=1) {
            this.positions[i] = {
                'id': i, // 1 - left, 2 - right, 3 - top, 4 - bottom
                'status' : false,
                'title': config.canvas.positions[i]['title'],
                'data' : _math.calcPlayerPosition(i),
            };
        }

        this.createBall();
        this.createTimer();
    }

    collideBall() {
        _math.forEach(this.positions, position => {
            if (this.state !== 'game') return;

            let k = 1;
            let positionId = position.id; // 1 - left, 2 - right, 3 - top, 4 - bottom
            let player = this.getPlayers('position', positionId)[0]; //get player with positionId
            let collideBounds = this.constructor.collideBounds(this.ball, positionId); //check collision ball with canvas bounds by positionId

            if (player) {
                if (collideBounds) {
                    player.incrementHealth();
                    !player.health && this.unsetPlayerPosition(positionId);
                    this.pauseGame();
                } else {
                    let intersects = this.constructor.intersects(this.ball, player.control); // check intersects ball with player control

                    if (intersects) {
                        this.ball.vel.x *= -k;

                        if (this.ball.vec.y < player.control.vec.y + (player.control.height/2)) {
                          this.ball.vel.y *= this.ball.direction.y < 0 ? -k : k;
                        } else if (this.ball.vec.y > player.control.vec.y + (player.control.height/2)) {
                          this.ball.vel.y *= this.ball.direction.y < 0 ? k : -k;
                        }
                    }
                }
            } else {
                if (collideBounds) {
                    if (positionId <= 2) {
                        this.ball.vel.x *= -1;
                    } else if (positionId > 2) {
                        this.ball.vel.y *= -1;
                    }
                }
            }
        });
    }

    setPlayerPosition(position) {
        if (!position || !this.positions[position] || this.positions[position]['status']) return false;
        this.positions[position]['status'] = true;
        return true;
    }

    unsetPlayerPosition(position) {
        if (position && this.positions[position]) this.positions[position]['status'] = false;
    }

    addPlayer(player) {
        player['roomName'] = this.name;
        this.players[player.id] = player;
    }

    removePlayer(playerId) {
        let player = this.players[playerId];
        if (!player) return false;

        this.unsetPlayerPosition(player.position);
        delete this.players[playerId];
    }

    resetPlayers() {
        _math.forEach(this.players, player => player.reset());
    }

    getPlayers(key, value) {
        let players = [];

        _math.forEach(this.players, player => (player[key] === value) && players.push(player) );

        return players;
    }

    startGame() {
        this.state = 'game';
        this.ball.enable();
        this.resetPlayers();
        this.timer.start();
    }

    pauseGame() {
        this.state = 'wait';
        this.ball.disable();
        this.ball.reset();

        let idlePlayers = this.getPlayers('state', 'idle');
        if (idlePlayers.length < config.minCountPlayersInRoom) {
            this.completeGame();
        } else {
            setTimeout(()=> {
                this.timer.reset();
                this.ball.enable();
                this.state = 'game';
            }, 1000);
        }
    }

    stopGame() {
        this.reset();
        this.update();
    }

    completeGame() {
        this.stopGame();

        _math.serverCallback(this.io.in(this.name), 'message', 'success', `${this.winPlayer.name} won the game!!!`);

        this.resetPlayers();
        this.update();
    }

    createBall() {
        let center = (config.canvas.size / 2) - (config.ball.size / 2);

        this.ball = new Ball(
            center,
            center,
            config.ball.size,
            config.ball.color,
        );
    }

    createTimer() {
        this.timer = new Timer();

        this.timer.update = (time) => {
            this.update();

            if (this.ball && this.ball.state === 'idle') {
                this.ball.update(time);
                this.collideBall();
            }
        };
    }

    update() {
        _math.serverCallback(this.io.in(this.name), 'update', 'success', this.data);
    }

    reset() {
        this.state = 'idle';
        this.timer.stop();
        this.ball.disable();
        this.ball.reset();
    }

    get countPlayers() {
        return Object.keys(this.players).length;
    }

    get winPlayer() {
        let idlePlayers = this.getPlayers('state', 'idle');

        return (idlePlayers.length === 1) ? idlePlayers[0] : null;
    }

    get data() {
        return {
            'state'     : this.state,
            'name'      : this.name,
            'players'   : this.players,
            'positions' : this.positions,
            'ball'      : this.ball,
        }
    }

    static intersects(circle, rect) {
        let deltaX = circle.vec.x - Math.max(rect.vec.x, Math.min(circle.vec.x, rect.vec.x + rect.width));
        let deltaY = circle.vec.y - Math.max(rect.vec.y, Math.min(circle.vec.y, rect.vec.y + rect.height));

        return (deltaX * deltaX + deltaY * deltaY) < (circle.r * circle.r);
    }

    static collideBounds(el, boundIndex) {
        return [
            (el.left <= 0),
            (el.right >= config.canvas.size),
            (el.top <= 0),
            (el.bottom >= config.canvas.size),
        ][boundIndex - 1];
    }
};
