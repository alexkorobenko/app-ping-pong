import Vue from 'vue';
import Vuex from 'vuex';
import io from 'socket.io-client';

Vue.use(Vuex);

const socketHost = process.env.NODE_ENV === 'development' ? process.env.VUE_APP_IO_HOST : '';

const store = new Vuex.Store({
    state: {
        activeComponent: 'Index',
        socket : io(socketHost),
        config: {},
        room: {},
        player: {},
        bot: {}, //temp bot data
    },

    actions: {
        updateActiveComponent ({commit}, payload) {
            commit('setActiveComponent', payload);
        },

        updateConfig ({commit}, payload) {
            commit('setConfig', payload);
        },

        updateRoom ({commit}, payload) {
            commit('setRoom', payload);
        },

        updatePlayer ({commit}, payload) {
            if (payload.role === 'player') {
                commit('setPlayer', payload);
            } else {
                commit('setBot', payload);
            }
        },
    },

    mutations: {
        setActiveComponent (state, payload) {
            state.activeComponent = payload;
        },

        setConfig (state, payload) {
            state.config = payload;
        },

        setRoom (state, payload) {
            state.room = payload;
        },

        setPlayer (state, payload) {
            state.player = payload;
        },

        setBot (state, payload) {
            state.bot = payload;
        },
    },

    getters: {
        activeComponent: (state) => {
            return state.activeComponent;
        },

        socket: (state) => {
            return state.socket;
        },

        config: (state) => {
            return state.config;
        },

        room: (state) => {
            return state.room;
        },

        player: (state) => {
            return state.player;
        },

        bot: (state) => {
            return state.bot;
        },

        readyPlayersCount: (state) => {
            let count = 0;
            let players = state.room.players;

            if (players) {
                for (let key in players) {
                    if (players.hasOwnProperty(key) && players[key].position) count += 1;
                }
            }

            return count;
        },
    }
});

export default store;
