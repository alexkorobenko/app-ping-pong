import Vue from 'vue';
import App from './App';
import Vuex from 'vuex';
import Vuelidate from 'vuelidate';
import store from './store/index';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;
Vue.use(Vuex, Vuelidate);

new Vue({
  render: h => h(App),
  vuetify,
  store
}).$mount('#app');
